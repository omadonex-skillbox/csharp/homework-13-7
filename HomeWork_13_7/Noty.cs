﻿using HomeWork_13_7.Data;
using HomeWork_13_7.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Position;
using ToastNotifications.Messages;

namespace HomeWork_13_7
{
    public class Noty
    {
        private Notifier notifier;

        public Noty()
        {
            notifier = new Notifier(cfg =>
            {
                cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.BottomRight,
                offsetX: 10,
                offsetY: 10);

                cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                    notificationLifetime: TimeSpan.FromSeconds(3),
                    maximumNotificationCount: MaximumNotificationCount.FromCount(5));

                cfg.Dispatcher = Application.Current.Dispatcher;
            });
        }

        public void OpenAccount(Account account, User user)
        {
            string message = $"Account # {account.Number} was opened by user: {user.GetType().Name}";
            notifier.ShowSuccess(message);
        }

        public void CloseAccount(Account account, User user)
        {
            string message = $"Account # {account.Number} was closed by user: {user.GetType().Name}";
            notifier.ShowWarning(message);
        }

        public void IncomeAccount(Account account, double money, User user)
        {
            string message = $"Account # {account.Number} recieved income {money}$ by user: {user.GetType().Name}";
            notifier.ShowInformation(message);
        }

        public void ConsumptionAccount(Account account, double money, User user)
        {
            string message = $"Account # {account.Number} recieved consumption {money}$ by user: {user.GetType().Name}";
            notifier.ShowInformation(message);
        }

        public void SendMoney(Account accountFrom, Account accountTo, double money, User user)
        {
            string message = $"Account # {accountFrom.Number} sended {money}$ to Account # {accountTo.Number} by user: {user.GetType().Name}";
            notifier.ShowWarning(message);
        }
    }
}
