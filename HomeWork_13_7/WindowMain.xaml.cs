﻿using HomeWork_13_7.Data;
using HomeWork_13_7.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HomeWork_13_7
{
    /// <summary>
    /// Interaction logic for WindowMain.xaml
    /// </summary>
    public partial class WindowMain : Window
    {
        public const string PathDataClient = "dataClient.txt";
        public const string PathDataAccount = "dataAccount.txt";
        public const string PathDataJournal = "dataJournal.txt";

        public User user;
        public JournalLog log;
        public Noty noty;

        public WindowMain()
        {
            InitializeComponent();
            user = new User(new Repository<Client>(PathDataClient), new Repository<Account>(PathDataAccount));
            log = new JournalLog(new Repository<Journal>(PathDataJournal));
            noty = new Noty();

            user.EventAccountOpened += log.OpenAccount;            
            user.EventAccountClosed += log.CloseAccount;
            user.EventAccountIncome += log.IncomeAccount;
            user.EventAccountConsumption += log.ConsumptionAccount;
            user.EventMoneySended += log.SendMoney;

            if (user.GetClientCount() == 0 && user.GetAccountCount() == 0)
            {
                FillTestData();
            }

            user.EventAccountOpened += noty.OpenAccount;
            user.EventAccountClosed += noty.CloseAccount;
            user.EventAccountIncome += noty.IncomeAccount;
            user.EventAccountConsumption += noty.ConsumptionAccount;
            user.EventMoneySended += noty.SendMoney;
        }

        private void User_EventAccountClosed(Account arg1, User arg2)
        {
            throw new NotImplementedException();
        }

        private void FillTestData()
        {
            user.AddClient(new Dictionary<string, object>()
            {
                { "firstName", "Client 1" },
                { "lastName", "A 1" },
                { "optName", "AA 1" },
                { "passportSeries", "aaa" },
                { "passportNumber", "11 11 11" },
                { "phoneNumber", "1234567890" },
            });
            user.AddClient(new Dictionary<string, object>()
            {
                { "firstName", "Client 2" },
                { "lastName", "B 2" },
                { "optName", "BB 2" },
                { "passportSeries", "bbb" },
                { "passportNumber", "22 22 22" },
                { "phoneNumber", "2345678901" },
            });
            user.AddClient(new Dictionary<string, object>()
            {
                { "firstName", "Client 3" },
                { "lastName", "C 3" },
                { "optName", "CC 3" },
                { "passportSeries", "ccc" },
                { "passportNumber", "33 33 33" },
                { "phoneNumber", "3456789012" },
            });
            user.AddAccount(1, true, true);
            user.AddAccount(1, false, true);
            user.AddAccount(2, true, true);
            user.AddAccount(3, false, true);
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            DataGridClient.ItemsSource = user.GetClientList();
        }

        private void DataGridClient_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Client client = (sender as DataGrid).SelectedItem as Client;
            ListBoxAccount.ItemsSource = user.GetAccountList(client.Id);
            bool hasDepositAccount = user.HasClientAccount(client.Id, true);
            bool hasNonDepositAccount = user.HasClientAccount(client.Id, false);

            ButtonDepositAccountAction.Content = hasDepositAccount ? "Delete deposit account" : "Add deposit account";
            ButtonNonDepositAccountAction.Content = hasNonDepositAccount ? "Delete non-deposit account" : "Add non-deposit account";
           
            ComboBoxAccountType.ItemsSource = user.GetAccountList(client.Id);
        }

        private void AccountAction(bool isDeposit)
        {
            Client client = DataGridClient.SelectedItem as Client;
            bool hasAccount = user.HasClientAccount(client.Id, isDeposit);
            if (hasAccount)
            {
                Account account = user.GetAccount(client.Id, isDeposit);
                user.DeleteAccount(account.Id);
            }
            else
            {
                user.AddAccount(client.Id, isDeposit, true);
            }
            ListBoxAccount.ItemsSource = user.GetAccountList(client.Id);
            if (isDeposit)
            {
                ButtonDepositAccountAction.Content = hasAccount ? "Add deposit account" : "Delete deposit account";
            }
            else
            {
                ButtonNonDepositAccountAction.Content = hasAccount ? "Add non-deposit account" : "Delete non-deposit account";
            }
        }

        private void ButtonDepositAccountAction_Click(object sender, RoutedEventArgs e)
        {
            AccountAction(true);
        }

        private void ButtonNonDepositAccountAction_Click(object sender, RoutedEventArgs e)
        {
            AccountAction(false);
        }

        private void ButtonIncome_Click(object sender, RoutedEventArgs e)
        {
            if (ComboBoxAccountType.SelectedIndex == -1)
            {
                MessageBox.Show("Select Account");
            } else
            {                
                user.IncomeToAccount((int)ComboBoxAccountType.SelectedValue);
            }
        }

        private void ButtonSend_Click(object sender, RoutedEventArgs e)
        {
            WindowSend wSend = new WindowSend(this);
            wSend.ShowDialog();
        }
    }
}
