﻿using HomeWork_13_7.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;

namespace HomeWork_13_7.Users
{
    public class User
    {
        protected Repository<Client> repositoryClient;
        protected Repository<Account> repositoryAccount;        

        public User(Repository<Client> repositoryClient, Repository<Account> repositoryAccount)
        {
            this.repositoryClient = repositoryClient;
            this.repositoryAccount = repositoryAccount;            
        }

        public event Action<Account, User> EventAccountOpened;
        public event Action<Account, User> EventAccountClosed;
        public event Action<Account, double, User> EventAccountIncome;
        public event Action<Account, double, User> EventAccountConsumption;
        public event Action<Account, Account, double, User> EventMoneySended;

        public ObservableCollection<Client> GetClientList()
        {
            return repositoryClient.All();
        }

        public int GetClientCount()
        {
            return repositoryClient.Count();
        }

        public ObservableCollection<Account> GetAccountList()
        {
            return repositoryAccount.All();
        }

        public int GetAccountCount()
        {
            return repositoryAccount.Count();
        }

        public ObservableCollection<Account> GetAccountList(int clientId)
        {
            return new ObservableCollection<Account>(repositoryAccount.All().Where(account => account.ClientId == clientId));            
        }

        public void AddClient(Dictionary<string, object> data)
        {
            repositoryClient.Add(this, data);
        }

        public Account AddAccount(int clientId, bool isDeposit, bool randomMoney)
        {
            Account account = repositoryAccount.Add(this, new Dictionary<string, object>()
            {
                { "number", Guid.NewGuid().ToString() },
                { "clientId", clientId },
                { "money", randomMoney ? Math.Round((new Random()).NextDouble() * 100000, 2) : 0 },
                { "isDeposit", isDeposit },
            });

            EventAccountOpened?.Invoke(account, this);

            return account;
        }

        public void IncomeToAccount(int accountId, double? money = null)
        {
            Account account = repositoryAccount.GetById(accountId);
            double moneyToIncome = money == null ? Math.Round((new Random()).NextDouble() * 1000, 2) : (double)money;
            repositoryAccount.UpdateById(this, accountId, new Dictionary<string, object>()
            {
                { "money", account.Money + moneyToIncome },
            });

            EventAccountIncome?.Invoke(account, moneyToIncome, this);
        }

        public void ConsumptionFromAccount(int accountId, double? money = null)
        {
            Account account = repositoryAccount.GetById(accountId);
            double moneyToConsumption = money == null ? Math.Round((new Random()).NextDouble() * 1000, 2) : (double)money;
            repositoryAccount.UpdateById(this, accountId, new Dictionary<string, object>()
            {
                { "money", account.Money - moneyToConsumption },
            });

            EventAccountConsumption?.Invoke(account, moneyToConsumption, this);
        }

        public void Send(int fromAccountId, int toAccountId, double money)
        {            
            ConsumptionFromAccount(fromAccountId, money);
            IncomeToAccount(toAccountId, money);

            EventMoneySended?.Invoke(repositoryAccount.GetById(fromAccountId), repositoryAccount.GetById(toAccountId), money, this);
        }

        public void DeleteAccount(int accountId)
        {
            Account account = repositoryAccount.GetById(accountId);
            repositoryAccount.DeleteById(this, accountId);
            EventAccountClosed?.Invoke(account, this);
        }

        public Account GetAccount(int accountId)
        {
            return repositoryAccount.GetById(accountId);
        }
        public Account GetAccount(int clientId, bool isDeposit) 
        {
            return repositoryAccount.All().First(item => item.ClientId == clientId && item.IsDeposit == isDeposit);
        }

        public bool HasClientAccount(int clientId, bool isDeposit)
        {
            return repositoryAccount.All().Where(item => item.ClientId == clientId && item.IsDeposit == isDeposit).Any();
        }
    }
}
