﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace HomeWork_13_7.Data
{
    public class Client : FillableRecord
    {
        [JsonProperty("firstName")]
        private string firstName;
        [JsonProperty("lastName")]
        private string lastName;
        [JsonProperty("optName")]
        private string optName;
        [JsonProperty("passportSeries")]
        private string passportSeries;
        [JsonProperty("passportNumber")]
        private string passportNumber;
        [JsonProperty("phoneNumber")]
        private string phoneNumber;        

        [JsonIgnore]
        public string FirstName { get { return firstName; } set { firstName = value; } }
        [JsonIgnore]
        public string LastName { get { return lastName; } set { lastName = value; } }
        [JsonIgnore]
        public string OptName { get { return optName; } set { optName = value; } }
        [JsonIgnore]
        public string PassportSeries { get { return passportSeries; } set { passportSeries = value; } }
        [JsonIgnore]
        public string PassportNumber { get { return passportNumber; } set { passportNumber = value; } }
        [JsonIgnore]
        public string PhoneNumber { get { return phoneNumber; } set { phoneNumber = value; } }            
    }
}
