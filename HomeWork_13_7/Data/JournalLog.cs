﻿using HomeWork_13_7.Data;
using HomeWork_13_7.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_13_7.Data
{
    public class JournalLog
    {
        public const int OperationIdOpenAccount = 1;
        public const int OperationIdCloseAccount = 2;
        public const int OperationIdIncomeAccount = 3;
        public const int OperationIdConsumptionAccount = 4;
        public const int OperationIdSendMoney = 5;

        protected Repository<Journal> repository;

        public JournalLog(Repository<Journal> repository)
        {            
            this.repository = repository;
        }

        public void OpenAccount(Account account, User user)
        {
            repository.Add(user, new Dictionary<string, object>()
            {
                { "number", account.Number },
                { "operationId", OperationIdOpenAccount },
                { "money", account.Money },
                { "userType", user.GetType().Name },
            });
        }

        public void CloseAccount(Account account, User user)
        {
            repository.Add(user, new Dictionary<string, object>()
            {
                { "number", account.Number },
                { "operationId", OperationIdCloseAccount },
                { "money", account.Money },
                { "userType", user.GetType().Name },
            });
        }

        public void IncomeAccount(Account account, double money, User user)
        {
            repository.Add(user, new Dictionary<string, object>()
            {
                { "number", account.Number },
                { "operationId", OperationIdIncomeAccount },
                { "money", money },
                { "userType", user.GetType().Name },
            });
        }

        public void ConsumptionAccount(Account account, double money, User user)
        {
            repository.Add(user, new Dictionary<string, object>()
            {
                { "number", account.Number },
                { "operationId", OperationIdConsumptionAccount },
                { "money", money },
                { "userType", user.GetType().Name },
            });
        }

        public void SendMoney(Account accountFrom, Account accountTo, double money, User user)
        {
            repository.Add(user, new Dictionary<string, object>()
            {
                { "number", accountFrom.Number },
                { "numberOpt", accountTo.Number },
                { "operationId", OperationIdSendMoney },
                { "money", money },
                { "userType", user.GetType().Name },
            });
        }
    }
}