﻿using HomeWork_13_7.Users;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_13_7.Data
{
    public class Journal : FillableRecord
    {
        [JsonProperty("number")]
        private string number;
        [JsonProperty("numberOpt")]
        private string? numberOpt;
        [JsonProperty("operationId")]
        private int operationId;
        [JsonProperty("money")]
        private double money;
        [JsonProperty("userType")]
        private string userType;
        [JsonIgnore]
        public string Number { get { return number; } set { number = value; } }
        [JsonIgnore]
        public string? NumberOpt { get { return numberOpt; } set { numberOpt = value; } }
        [JsonIgnore]
        public int OperationId { get { return operationId; } set { operationId = value; } }
        [JsonIgnore]
        public double Money { get { return money; } set { money = value; } }
        [JsonIgnore]
        public string UserType { get { return userType; } set { userType = value; } }
    }
}
